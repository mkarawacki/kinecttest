﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;
using System.Management;
using Microsoft.Samples.Kinect.WpfViewers;
using Microsoft.Kinect.Toolkit;
using MonoBrick.EV3;
using System.IO.Ports;
using MonoBrick.NXT;
using InTheHand.Net;
using InTheHand.Net.Sockets;
using Coding4Fun.Kinect.Wpf;
using System.Windows.Controls.Ribbon;
using InTheHand.Net.Bluetooth;
using System.Globalization;
namespace KinectTest
{

    
    
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RibbonWindow
    {
#region parametry - zmienne i stałe
        KinectSensor kinect;
        BluetoothClient client;
        bool closing = false;
        const int skeletonCount = 6;
        
        Skeleton[] allSkeletons = new Skeleton[skeletonCount];
        string mindstorms = "";
        string conntype = "";
        MonoBrick.EV3.Brick<MonoBrick.EV3.Sensor,MonoBrick.EV3.Sensor,MonoBrick.EV3.Sensor,MonoBrick.EV3.Sensor> ev3Brick;
        MonoBrick.NXT.Brick<MonoBrick.NXT.Sensor,MonoBrick.NXT.Sensor,MonoBrick.NXT.Sensor,MonoBrick.NXT.Sensor> nxtBrick;
        Dictionary<string, BluetoothAddress> BTdict = new Dictionary<string, BluetoothAddress>();
        BluetoothAddress targetmac = null;
        BluetoothEndPoint localEndpoint;
        string DEVICE_PIN="1234";
        /// <summary>
        /// Zapytanie do WMI wyszukuje wirtualne porty COM, których realne urządzenia mają adres MAC z puli 001653* czyli puli LEGO NXT / EV3
        /// </summary>
        string WMIqueryPL = "SELECT * FROM Win32_PnPEntity WHERE Caption like 'Standardowy port szeregowy przez link Bluetooth (COM%' and PNPDeviceID like '%&001653%'";
        string WMIqueryEN = "SELECT * FROM Win32_PnPEntity WHERE Caption like 'Standard Serial over Bluetooth link (COM%' and PNPDeviceID like '%&001653%'";
        string WMIquery;           
#endregion
        public MainWindow()
        {
            InitializeComponent();
            /// inicjalizacja adaptera o podanym adresie MAC na komputerze
            /// MAC adaptera LEGO: 00:0C:78:78:7b:f7
            localEndpoint = new BluetoothEndPoint(BluetoothAddress.Parse("000c78787bf7"), BluetoothService.SerialPort);
            CultureInfo ci = CultureInfo.InstalledUICulture;
            if (ci.Name.StartsWith("en"))
            {
                WMIquery = WMIqueryEN;
            }
            else WMIquery = WMIqueryPL;
        }
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param> 

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            StopKinect(kinect);
            if (mindstorms == "nxt" && nxtBrick!=null)
            {
                nxtBrick.Vehicle.Brake();
                nxtBrick.Connection.Close();
            }
            else if (mindstorms=="ev3" && ev3Brick!=null)
            {
                ev3Brick.Vehicle.Brake();
                ev3Brick.Connection.Close();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
           /* if (KinectSensor.KinectSensors.Count > 0) 
            {
                kinect = KinectSensor.KinectSensors[0];
                if (kinect.Status == KinectStatus.Connected) 
                {
                    kinect.ColorStream.Enable();
                    kinect.DepthStream.Enable();
                    kinect.SkeletonStream.Enable();
                    kinect.AllFramesReady +=kinect_AllFramesReady;
                    kinect.Start();
                }
            }*/
            ContentControl contentControl = FindVisualChildByName<ContentControl>(Wstega, "mainItemsPresenterHost");
            contentControl.Visibility = System.Windows.Visibility.Collapsed;
            var chooser = new KinectSensorChooser();
            chooser.KinectChanged += KinectSensorChooserKinectChanged;

            KinectChooser.KinectSensorChooser = chooser;
            chooser.Start();
            

        }

        /// <summary>
        /// Selektor Kinecta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KinectSensorChooserKinectChanged(object sender, KinectChangedEventArgs e)
        {
            //throw new NotImplementedException();
            KinectSensor OldSensor = (KinectSensor)e.OldSensor;
            StopKinect(OldSensor);
            KinectSensor NewSensor = (KinectSensor)e.NewSensor;
           //INICJALIZACJA POSZCZEGÓLNYCH STRUMIENI DANYCH / FUNKCJONALNOŚCI

            var parametry = new TransformSmoothParameters
            {
                Smoothing = 0.3f,
                Correction = 0.0f,
                Prediction = 0.0f,
                JitterRadius = 1.0f,
                MaxDeviationRadius = 0.5f
            };
            NewSensor.SkeletonStream.Enable(parametry);
            NewSensor.SkeletonStream.Enable();
            //NewSensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
            NewSensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
            
            NewSensor.AllFramesReady += kinect_AllFramesReady;
            try
            {
                NewSensor.Start();
            }
            catch (System.IO.IOException)
            {

                KinectChooser.KinectSensorChooser.TryResolveConflict();
            }
        }


        /// <summary>
        /// Zatrzymywanie pobierania danych z Kinecta
        /// </summary>
        /// <param name="sensor"></param>
        void StopKinect(KinectSensor sensor) 
        {
            if (sensor != null) 
            {
                if (sensor.IsRunning)
                {
                    //stop sensor 
                    sensor.Stop();

                    //stop audio if not null
                    if (sensor.AudioSource != null)
                    {
                        sensor.AudioSource.Stop();
                    }


                }
            } 
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void kinect_AllFramesReady(object sender, AllFramesReadyEventArgs e)
        {
            //throw new NotImplementedException();
            //using (ColorImageFrame colorFrame = e.OpenColorImageFrame()) 
            //{
            //    if (colorFrame == null) 
            //    {
            //        return;
            //    }
            //    byte[] pixels = new byte[colorFrame.PixelDataLength];
            //    colorFrame.CopyPixelDataTo(pixels);
            //    int stride = colorFrame.Width*4;
            //    KinectImgFrame.Source = BitmapSource.Create(colorFrame.Width,colorFrame.Height,96,96,PixelFormats.Bgr32,null,pixels,stride);
            //}
            if (closing)
            {
                return;
            }

            //Get a skeleton
            Skeleton first = GetFirstSkeleton(e);

            if (first == null)
            {
                if (mindstorms == "ev3") 
                {
                    LMotorPwrLabel.Content = "0";
                    LMotorPwrSlider.Value = 0;
                    RmotorPwrLabel.Content = "0";
                    RMotorPwrSlider.Value = 0;
                    ev3Brick.MotorB.Brake();
                    ev3Brick.MotorC.Brake();

                }
                if (mindstorms == "nxt")
                {
                    LMotorPwrLabel.Content = "0";
                    LMotorPwrSlider.Value = 0;
                    RmotorPwrLabel.Content = "0";
                    RMotorPwrSlider.Value = 0;
                    nxtBrick.MotorB.Brake();
                    nxtBrick.MotorC.Brake();

                }
                return;

            }



            //set scaled position
            //ScalePosition(headImage, first.Joints[JointType.Head]);
            //ScalePosition(leftEllipse, first.Joints[JointType.HandLeft]);
            //ScalePosition(rightEllipse, first.Joints[JointType.HandRight]);

            GetCameraPoint(first, e); 
        }

        void GetCameraPoint(Skeleton first, AllFramesReadyEventArgs e)
        {

            using (DepthImageFrame depth = e.OpenDepthImageFrame())
            {
                if (depth == null ||
                    KinectChooser.KinectSensorChooser.Kinect == null)
                {
                    return;
                }


                //Map a joint location to a point on the depth map
                //head
                DepthImagePoint headDepthPoint =
                    depth.MapFromSkeletonPoint(first.Joints[JointType.Head].Position);
                //left hand
                DepthImagePoint leftDepthPoint =
                    depth.MapFromSkeletonPoint(first.Joints[JointType.HandLeft].Position);
                //right hand
                DepthImagePoint rightDepthPoint =
                    depth.MapFromSkeletonPoint(first.Joints[JointType.HandRight].Position);


                //Map a depth point to a point on the color image
                //head
                ColorImagePoint headColorPoint =
                    depth.MapToColorImagePoint(headDepthPoint.X, headDepthPoint.Y,
                    ColorImageFormat.RgbResolution640x480Fps30);
                //left hand
                ColorImagePoint leftColorPoint =
                    depth.MapToColorImagePoint(leftDepthPoint.X, leftDepthPoint.Y,
                    ColorImageFormat.RgbResolution640x480Fps30);
                //right hand
                ColorImagePoint rightColorPoint = 
                    depth.MapToColorImagePoint(rightDepthPoint.X, rightDepthPoint.Y,
                    ColorImageFormat.RgbResolution640x480Fps30);


                //Set location
                //CameraPosition(headImage, headColorPoint);
                //CameraPosition(leftEllipse, leftColorPoint);
               // CameraPosition(rightEllipse, rightColorPoint);
                /*
                 * Do modyfikacji - skalowanie położenia rąk względem położenia
                 * referencyjnego na przedział [-100, 100]
                 * 
                 * Do dodania - sterowanie silnikiem gimbala kamery/smartfona
                 * theta = arccos y/(sqrt(y^2 + z^2))
                 */
                LMotorPwrSlider.Value = Math.Floor(-(first.Joints[JointType.HandLeft].Position.Z - first.Joints[JointType.HipLeft].Position.Z) * 100);
                RMotorPwrSlider.Value = Math.Floor(-100 * (first.Joints[JointType.HandRight].Position.Z - first.Joints[JointType.HipRight].Position.Z));
                LMotorPwrLabel.Content = Math.Floor(-(first.Joints[JointType.HandLeft].Position.Z - first.Joints[JointType.HipLeft].Position.Z) * 100);
                RmotorPwrLabel.Content = Math.Floor(-(first.Joints[JointType.HandRight].Position.Z-first.Joints[JointType.HipRight].Position.Z)*100);
                /*if (LMotorPwrSlider.Value < 0 && mindstorms == "ev3") ev3Brick.MotorB.Reverse = true;
                else if (LMotorPwrSlider.Value > 0 && mindstorms == "ev3") ev3Brick.MotorB.Reverse = false;
                else if (LMotorPwrSlider.Value < 0 && mindstorms == "nxt") nxtBrick.MotorB.Reverse = true;
                else if (LMotorPwrSlider.Value > 0 && mindstorms == "nxt") nxtBrick.MotorB.Reverse = false;
                else if (RMotorPwrSlider.Value < 0 && mindstorms == "ev3") ev3Brick.MotorC.Reverse = true;
                else if (RMotorPwrSlider.Value > 0 && mindstorms == "ev3") ev3Brick.MotorC.Reverse = false;
                else if (RMotorPwrSlider.Value < 0 && mindstorms == "nxt") nxtBrick.MotorC.Reverse = true;
                else if (RMotorPwrSlider.Value > 0 && mindstorms == "nxt") nxtBrick.MotorC.Reverse=false;
                */if (mindstorms == "ev3")
                  {
                      ev3Brick.MotorB.On((sbyte)LMotorPwrSlider.Value);
                      ev3Brick.MotorC.On((sbyte)RMotorPwrSlider.Value);
                      
                  }
                  else if (mindstorms == "nxt")
                  {
                      nxtBrick.MotorB.On((sbyte)LMotorPwrSlider.Value);
                      nxtBrick.MotorC.On((sbyte)RMotorPwrSlider.Value);
                  }
                if (Math.Abs(first.Joints[JointType.ElbowLeft].Position.Z - first.Joints[JointType.HipLeft].Position.Z) < 0.05 && Math.Abs(first.Joints[JointType.HandLeft].Position.Z - first.Joints[JointType.HipLeft].Position.Z) < 0.05) 
                {
                    LMotorPwrSlider.Value =  0;
                    LMotorPwrLabel.Content = 0;
                    if (mindstorms == "ev3") 
                    {
                        ev3Brick.MotorB.Brake();
                    }
                    else if(mindstorms=="nxt")
                    {
                        nxtBrick.MotorB.Brake();
                    }
                }

                if (Math.Abs(first.Joints[JointType.ElbowRight].Position.Z - first.Joints[JointType.HipRight].Position.Z) < 0.05 && Math.Abs(first.Joints[JointType.HandRight].Position.Z - first.Joints[JointType.HipRight].Position.Z) < 0.05)
                {
                    RMotorPwrSlider.Value = 0;
                    RmotorPwrLabel.Content = 0;
                    if (mindstorms == "ev3")
                    {
                        ev3Brick.MotorC.Brake();
                    }
                    else if (mindstorms == "nxt")
                    {
                        nxtBrick.MotorC.Brake();
                    }
                }
            }
        }

        private void CameraPosition(FrameworkElement element, ColorImagePoint point)
        {
            //Divide by 2 for width and height so point is right in the middle 
            // instead of in top/left corner
            Canvas.SetLeft(element, point.X - element.Width / 2);
            Canvas.SetTop(element, point.Y - element.Height / 2);

        }

       /* private void ScalePosition(FrameworkElement element, Joint joint)
        {
            //convert the value to X/Y
            //Joint scaledJoint = joint.ScaleTo(1280, 720); 

            //convert & scale (.3 = means 1/3 of joint distance)
            Joint scaledJoint = joint.ScaleTo(1280, 720, .3f, .3f);

            Canvas.SetLeft(element, scaledJoint.Position.X);
            Canvas.SetTop(element, scaledJoint.Position.Y);

        }*/

        Skeleton GetFirstSkeleton(AllFramesReadyEventArgs e)
        {
            using (SkeletonFrame skeletonFrameData = e.OpenSkeletonFrame())
            {
                if (skeletonFrameData == null)
                {
                    return null;
                }


                skeletonFrameData.CopySkeletonDataTo(allSkeletons);

                //get the first tracked skeleton
                Skeleton first = (from s in allSkeletons
                                  where s.TrackingState == SkeletonTrackingState.Tracked
                                  select s).FirstOrDefault();

                return first;

            }
        }

        private void KinectSensorChooserUI_Loaded(object sender, RoutedEventArgs e)
        {

        }
        #region Nawigacja okna przeglądarki strumienia wideo
        private void IPCamConnectBtn_Click(object sender, RoutedEventArgs e)
        {
            try { IPCamStream.Navigate(new Uri(IPCamURL.Text)); }
            catch (Exception exc) { MessageBox.Show(exc.Message); }
        }
        private void IPCamStream_Navigating(object sender, System.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            IPCamURL.Text = e.Uri.OriginalString;
        }

        private void BrowseBack_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ((IPCamStream != null) && (IPCamStream.CanGoBack));
        }

        private void BrowseBack_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            IPCamStream.GoBack();
        }

        private void BrowseForward_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ((IPCamStream != null) && (IPCamStream.CanGoForward));
        }

        private void BrowseForward_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            IPCamStream.GoForward();
        }

        private void GoToPage_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void GoToPage_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (IPCamURL.Text.StartsWith("http://"))
                IPCamStream.Navigate(IPCamURL.Text);
            else IPCamStream.Navigate("http://" + IPCamURL.Text);
        }

        #endregion

        
        /// <summary>
        /// Łączenie z wybranym urządzeniem BT
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BTConnBtn_Click(object sender, RoutedEventArgs e)
        {

        }
        /// <summary>
        /// Reakcja na wybór urządzenia Bluetooth z listy dostępnych
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BTDevList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (conntype == "bt" && USBConnection.IsChecked==false)
            {
                var mac = BTdict[BTDevList.SelectedItem.ToString()];
                targetmac = mac;
                MessageBox.Show("Adres MAC urządzenia Bluetooth: " + mac);
            }
        }
        

        private void ToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            ContentControl contentControl = FindVisualChildByName<ContentControl>(Wstega, "mainItemsPresenterHost");
            contentControl.Visibility = System.Windows.Visibility.Collapsed;
            ImageBrush bottomBrush = new ImageBrush();
            bottomBrush.ImageSource =
                new BitmapImage(
                    new Uri(@"../../Icons/16/go-bottom.png", UriKind.Relative)
                );
            SchowajPokazWstege.Background = bottomBrush;
            SchowajPokazWstege.FocusedBackground = bottomBrush;
            SchowajPokazWstege.MouseOverBackground = bottomBrush;
            SchowajPokazWstege.PressedBackground = bottomBrush;
            //ToggleBtnBg.ImageSource = new BitmapImage(new Uri(@"../../Icons/16/go-bottom.png", UriKind.Relative));
        }

        private void ToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            ContentControl contentControl = FindVisualChildByName<ContentControl>(Wstega, "mainItemsPresenterHost");
            contentControl.Visibility = System.Windows.Visibility.Visible;
            //SchowajPokazWstege.Content = "^";
            ImageBrush topBrush = new ImageBrush();
            topBrush.ImageSource =
                new BitmapImage(
                    new Uri(@"../../Icons/16/go-top.png", UriKind.Relative)
                );
            SchowajPokazWstege.Background = topBrush;
            SchowajPokazWstege.FocusedBackground = topBrush;
            SchowajPokazWstege.MouseOverBackground = topBrush;
            SchowajPokazWstege.PressedBackground = topBrush;
            //ToggleBtnBg.ImageSource = new BitmapImage(new Uri(@"../../Icons/16/go-top.png",UriKind.Relative));
            //SchowajPokazWstege.Content=
               
        }

        private T FindVisualChildByName<T>(DependencyObject parent, string name) where T : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                string controlName = child.GetValue(Control.NameProperty) as string;
                if (controlName == name)
                {
                    return child as T;
                }
                else
                {
                    T result = FindVisualChildByName<T>(child, name);
                    if (result != null)
                        return result;
                }
            }
            return null;
        }

        /// <summary>
        /// Wychodzenie z programu przez menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitItem_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Czy na pewno chcesz zakończyć pracę programu?", "Potwierdzenie zamknięcia programu", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes) this.Close();
        }

        private void EstablishConnectionBtn_Click(object sender, RoutedEventArgs e)
        {
            if (conntype == "bt" && mindstorms == "ev3")
            {
                //ev3Brick.Connection.Open()
                PairAndConnectBTDev();
                string COMport;  
                
                System.Management.ManagementObjectSearcher Searcher = new System.Management.ManagementObjectSearcher(WMIquery);
                System.Management.ManagementObjectCollection Porty = Searcher.Get();
                System.Management.ManagementObject Port = Porty.OfType<ManagementObject>().First();
                COMport=extractCOMregex(Port.GetPropertyValue("Caption").ToString());
                
                try
                {
                    ev3Brick = new MonoBrick.EV3.Brick<MonoBrick.EV3.Sensor, MonoBrick.EV3.Sensor, MonoBrick.EV3.Sensor, MonoBrick.EV3.Sensor>(COMport);
                    ev3Brick.Connection.Open();
                }
                catch (MonoBrick.ConnectionException monoexc) 
                {
                    MessageBox.Show("Wystąpił błąd w trakcie próby połączenia z wybranym Mindstorms EV3: \n" + monoexc.Message, "Błąd połączenia z Mindstorms");
                }

                    
            }
            else if (conntype == "bt" && mindstorms == "nxt")
            {
                PairAndConnectBTDev();
                PairAndConnectBTDev();
                string COMport;
                
                System.Management.ManagementObjectSearcher Searcher = new System.Management.ManagementObjectSearcher(WMIquery);
                System.Management.ManagementObjectCollection Porty = Searcher.Get();
                System.Management.ManagementObject Port = Porty.OfType<ManagementObject>().First();
                COMport = extractCOMregex(Port.GetPropertyValue("Caption").ToString());
                nxtBrick = new MonoBrick.NXT.Brick<MonoBrick.NXT.Sensor, MonoBrick.NXT.Sensor, MonoBrick.NXT.Sensor, MonoBrick.NXT.Sensor>(COMport);
                nxtBrick.Connection.Open();
            }
            else if (conntype == "usb" && mindstorms == "ev3")
            {
                ev3Brick = new MonoBrick.EV3.Brick<MonoBrick.EV3.Sensor, MonoBrick.EV3.Sensor, MonoBrick.EV3.Sensor, MonoBrick.EV3.Sensor>("usb");
                ev3Brick.Connection.Open();
            }

            else if (conntype == "usb" && mindstorms == "nxt")
            {
                nxtBrick = new MonoBrick.NXT.Brick<MonoBrick.NXT.Sensor, MonoBrick.NXT.Sensor, MonoBrick.NXT.Sensor, MonoBrick.NXT.Sensor>("usb");
                nxtBrick.Connection.Open();
            }
            else MessageBox.Show("Wybierz typ Mindstorms i metodę połączenia!", "Błąd ustawień połączenia", MessageBoxButton.OK, MessageBoxImage.Exclamation);

        }

        private void Connect(IAsyncResult result)
        {
            if (result.IsCompleted)
            {
                // client is connected now :)
                MessageBox.Show("Udało się !!!");
                /*COMPortsDevList.Items.Clear();
                System.Management.ManagementObjectSearcher Searcher = new System.Management.ManagementObjectSearcher(WMIquery);
                foreach (System.Management.ManagementObject Port in Searcher.Get())
                {
                    COMPortsDevList.Items.Add(Port.ToString());//.GetPropertyValue("Name").ToString() + ": " + Port.GetPropertyValue("DeviceID").ToString() + "--" + Port.GetPropertyValue("PNPDeviceID").ToString());


                }*/
            }
        }


        private void BTConnection_Checked(object sender, RoutedEventArgs e)
        {
            BTDevList.Items.Clear();
            BTGroup.Visibility = Visibility.Visible;
            //BTScanForDevices();
            try 
            {
                client = new BluetoothClient(localEndpoint);
                BluetoothComponent localComponent = new BluetoothComponent(client);
            // async methods, can be done synchronously too
                localComponent.DiscoverDevicesAsync(255, true, true, true, true, null);
                localComponent.DiscoverDevicesProgress += new EventHandler<DiscoverDevicesEventArgs>(component_DiscoverDevicesProgress);
                localComponent.DiscoverDevicesComplete += new EventHandler<DiscoverDevicesEventArgs>(component_DiscoverDevicesComplete);
            }
            catch (PlatformNotSupportedException exc) 
            
            {
                MessageBoxResult result =  MessageBox.Show("Brak dostępnego adaptera Bluetooth. Sprawdź połączenie i spróbuj ponownie", "Błąd połączenia Bluetooth", MessageBoxButton.YesNo, MessageBoxImage.Error);
                if (result == MessageBoxResult.No) this.Close();
            }
            

            USBConnection.IsChecked = false;

            conntype = "bt";
            //MessageBox.Show("wybrano połączenie Bluetooth");
        }

private void component_DiscoverDevicesProgress(object sender, DiscoverDevicesEventArgs e)
{
    // log and save all found devices
    for (int i = 0; i < e.Devices.Length; i++)
    {           
        if (e.Devices[i].Remembered)
        {
            Console.WriteLine(e.Devices[i].DeviceName + " (" + e.Devices[i].DeviceAddress + "): Device is known");
        }
        else
        {
            Console.WriteLine(e.Devices[i].DeviceName + " (" + e.Devices[i].DeviceAddress + "): Device is unknown");
        }
        this.BTDevList.Items.Add(e.Devices[i].DeviceName.ToString());
        if (conntype == "bt" && USBConnection.IsChecked == false && !BTdict.ContainsKey(e.Devices[i].DeviceName)&&!BTdict.ContainsValue(e.Devices[i].DeviceAddress)) BTdict.Add(e.Devices[i].DeviceName, e.Devices[i].DeviceAddress); 
    }
}

private void component_DiscoverDevicesComplete(object sender, DiscoverDevicesEventArgs e)
{
    // log some stuff
}

        private void BTConnection_Unchecked(object sender, RoutedEventArgs e)
        {
            BTDevList.Items.Clear();
            conntype = "";
            //MessageBox.Show("Bluetooth unchecked");
        }

        private void USBConnection_Unchecked(object sender, RoutedEventArgs e)
        {
            conntype = "";
            //MessageBox.Show("USB unchecked");
        }

        private void USBConnection_Checked(object sender, RoutedEventArgs e)
        {
            conntype = "usb";
            BTConnection.IsChecked = false;
            //MessageBox.Show("wybrano połączenie USB");
        }
        

        private void NXTRadioBtn_Checked(object sender, RoutedEventArgs e)
        {
            mindstorms = "nxt";
        }

        private void NXTRadioBtn_Unchecked(object sender, RoutedEventArgs e)
        {
            mindstorms = "";
        }

        private void EV3RadioBtn_Unchecked(object sender, RoutedEventArgs e)
        {
            mindstorms = "";
        }

        private void EV3RadioBtn_Checked(object sender, RoutedEventArgs e)
        {
            mindstorms = "ev3";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="caption">Wynik zapytania WMIquery zawierający nazwę COMportu</param>
        /// <returns>zwraca z podanego zapytania tylko frazę COMx jako string</returns>
        private string extractCOMregex(string caption) 
        {
            string comPort = caption.Substring(caption.LastIndexOf("(COM")).Replace("(",string.Empty).Replace(")",string.Empty);
            return comPort;

        }
        /// <summary>
        /// parowanie i łączenie z urządzeniem Bluetooth
        /// </summary>
        private void PairAndConnectBTDev()
        {
            BluetoothDeviceInfo[] paired = client.DiscoverDevices(255, false, true, false, false);
            BluetoothDeviceInfo device = new BluetoothDeviceInfo(targetmac);
            bool isPaired = false;
            for (int i = 0; i < paired.Length; i++)
            {
                if (device.Equals(paired[i]))
                {
                    isPaired = true;
                    break;
                }
            }

            // if the device is not paired, pair it!
            if (!isPaired)
            {
                // replace DEVICE_PIN here, synchronous method, but fast
                isPaired = BluetoothSecurity.PairRequest(device.DeviceAddress, DEVICE_PIN);
                if (isPaired)
                {
                    // now it is paired
                    if (device.Authenticated)
                    {
                        // set pin of device to connect with
                        client.SetPin(DEVICE_PIN);
                        // async connection method
                        client.BeginConnect(device.DeviceAddress, BluetoothService.SerialPort, new AsyncCallback(Connect), device);
                    }
                }
                else
                {
                    // pairing failed
                    MessageBox.Show("Próba parowania urządzeń zakończona fiaskiem!");
                }
            }

        }


        /*
        private void CalculateTiltAngle()
        {
            var tiltangle = 90 - Math.Atan(App.distance / (App.height - App.UsrHeight)) * 180.0 / Math.PI;///(Math.Sqrt(Math.Pow(distance,2)+Math.Pow(height,2))));
            if (tiltangle > 90) tiltangle -= 180;
            tiltangle *= -1;
            if (tiltangle < -27 || tiltangle > 27)
            {
                KinectTiltAngleLabel.Foreground = Brushes.Red;
                //sensor.ElevationAngle = 0;
            }
            else
            {
                KinectTiltAngleLabel.Foreground = Brushes.Black;
                //dodać ustawianie kąta nachylenia Kinecta
                App.theta = tiltangle;
                //sensor.ElevationAngle = (int)Math.Floor(tiltangle);
            }
            KinectTiltAngleLabel.Content = "Nachylenie czujnika względem poziomu [stopnie]: " + tiltangle.ToString();
            KinectHypotenuseLabel.Content = "Odległość użytkownika od czujnika w linii prostej [m]: " + Math.Sqrt(Math.Pow(App.distance, 2) + Math.Pow(App.height - App.UsrHeight, 2));
            #region rysowanie geometrii stanowiska
            Podstawa.X1 = 50;
            Podstawa.X2 = Podstawa.X1 + 100 * App.distance;
            Podstawa.Y2 = Podstawa.Y1 = 100;
            Wysokosc.X1 = Wysokosc.X2 = Podstawa.X1;
            Wysokosc.Y1 = Podstawa.Y1;
            Wysokosc.Y2 = Wysokosc.Y1 + 100 * App.height;
            Podstawa2.X1 = Podstawa.X1;
            Podstawa2.X2 = Podstawa.X2;
            Podstawa2.Y1 = Podstawa2.Y2 = Wysokosc.Y2;
            Przeciwprostokatna.X1 = Podstawa.X1;
            Przeciwprostokatna.X2 = Podstawa.X2;
            Przeciwprostokatna.Y1 = Podstawa.Y1;
            Przeciwprostokatna.Y2 = Podstawa.Y1 + 100 * (App.height - App.UsrHeight);
            liniauzup.X1 = liniauzup.X2 = Podstawa.X2;
            liniauzup.Y1 = Podstawa.Y1;
            liniauzup.Y2 = Podstawa.Y1 + 100 * (App.height - App.UsrHeight);
            liniauzup.Stroke = Brushes.DarkGray;
            userline.X1 = userline.X2 = Podstawa.X2;
            userline.Y1 = Wysokosc.Y2;
            userline.Y2 = userline.Y1 - 100 * App.UsrHeight;
            userline.Stroke = Brushes.Navy;
            #endregion
            /* AngleArc.StartPoint = new Point(Podstawa.X1 + 50,Podstawa.Y1);
            AngleSegment.SweepDirection = SweepDirection.Clockwise;
            AngleSegment.Point = new Point(Podstawa.X1+50*(1-Math.Cos((90-tiltangle)*Math.PI/180.0)), Podstawa.Y1+50*(1+Math.Sin(tiltangle*Math.PI/180.0)));
            AngleSegment.Size = new Size(20, 20);
            
            
        }
    
        private void UserDistanceSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            App.distance = UserDistanceSlider.Value;
            CalculateTiltAngle();
        }


        private void SensorHeightSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            App.height = SensorHeightSlider.Value;
            CalculateTiltAngle();
        }

        private void UserHeightSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            App.UsrHeight = UserHeightSlider.Value;
            CalculateTiltAngle();
        }
        private void Expander_MouseEnter(object sender, MouseEventArgs e)
        {
            if (!expander.IsExpanded) expander.IsExpanded = true;

        }

        private void Expander_MouseLeave(object sender, MouseEventArgs e)
        {

        }

        private void ConfigItem_Click(object sender, RoutedEventArgs e)
        {
            expander.IsExpanded = true;
        }*/
    }
}
