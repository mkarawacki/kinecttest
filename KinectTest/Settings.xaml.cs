﻿using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KinectTest
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        
        //KinectSensor sensor;
        public Settings()
        {
            InitializeComponent();
            KinectTiltAngleLabel.Content = "Nachylenie czujnika względem poziomu [stopnie]: ";
            KinectHypotenuseLabel.Content = "Odległość użytkownika od czujnika w linii prostej [m]: ";
            UserHeightSlider.Value = App.UsrHeight;
            SensorHeightSlider.Value = App.height;
            UserDistanceSlider.Value = App.distance;

        }



        private void CalculateTiltAngle() 
        {
            var tiltangle = 90 - Math.Atan(App.distance / (App.height-App.UsrHeight) )*180.0/Math.PI;///(Math.Sqrt(Math.Pow(distance,2)+Math.Pow(height,2))));
            if (tiltangle > 90) tiltangle -= 180;
            tiltangle *= -1;
            if (tiltangle < -27 || tiltangle > 27)
            {
                KinectTiltAngleLabel.Foreground = Brushes.Red;
                //sensor.ElevationAngle = 0;
            }
            else
            {
                KinectTiltAngleLabel.Foreground = Brushes.Black;
                //dodać ustawianie kąta nachylenia Kinecta
                App.theta = tiltangle;
                //sensor.ElevationAngle = (int)Math.Floor(tiltangle);
            }
                KinectTiltAngleLabel.Content = "Nachylenie czujnika względem poziomu [stopnie]: " + tiltangle.ToString();
            KinectHypotenuseLabel.Content = "Odległość użytkownika od czujnika w linii prostej [m]: "+Math.Sqrt(Math.Pow(App.distance, 2) + Math.Pow(App.height-App.UsrHeight, 2));
            #region rysowanie geometrii stanowiska
            Podstawa.X1 = 50;
            Podstawa.X2 = Podstawa.X1 + 100 * App.distance;
            Podstawa.Y2 = Podstawa.Y1 = 100 ;
            Wysokosc.X1 = Wysokosc.X2 = Podstawa.X1;
            Wysokosc.Y1 = Podstawa.Y1;
            Wysokosc.Y2 = Wysokosc.Y1 + 100 * App.height;
            Podstawa2.X1 = Podstawa.X1;
            Podstawa2.X2 = Podstawa.X2;
            Podstawa2.Y1 = Podstawa2.Y2 = Wysokosc.Y2;
            Przeciwprostokatna.X1 = Podstawa.X1;
            Przeciwprostokatna.X2 = Podstawa.X2;
            Przeciwprostokatna.Y1 = Podstawa.Y1;
            Przeciwprostokatna.Y2 = Podstawa.Y1 + 100 * (App.height - App.UsrHeight);
            liniauzup.X1 =liniauzup.X2= Podstawa.X2;
            liniauzup.Y1 = Podstawa.Y1;
            liniauzup.Y2 = Podstawa.Y1 + 100 * (App.height - App.UsrHeight);
            liniauzup.Stroke=Brushes.DarkGray;
            userline.X1 = userline.X2= Podstawa.X2;
            userline.Y1 = Wysokosc.Y2;
            userline.Y2 = userline.Y1 - 100 * App.UsrHeight;
            userline.Stroke = Brushes.Navy;
            #endregion
            /* AngleArc.StartPoint = new Point(Podstawa.X1 + 50,Podstawa.Y1);
            AngleSegment.SweepDirection = SweepDirection.Clockwise;
            AngleSegment.Point = new Point(Podstawa.X1+50*(1-Math.Cos((90-tiltangle)*Math.PI/180.0)), Podstawa.Y1+50*(1+Math.Sin(tiltangle*Math.PI/180.0)));
            AngleSegment.Size = new Size(20, 20);
            
            */
        }

        private void UserDistanceSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            App.distance = UserDistanceSlider.Value;
            CalculateTiltAngle();
        }


        private void SensorHeightSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
           App.height = SensorHeightSlider.Value;
            CalculateTiltAngle();
        }

        private void UserHeightSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            App.UsrHeight = UserHeightSlider.Value;
            CalculateTiltAngle();
        }

        private void KinectChooser_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void SettingsWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var chooser = new KinectSensorChooser();
            chooser.KinectChanged += KinectSensorChooserKinectChanged;

            //KinectChooser.KinectSensorChooser = chooser;
            chooser.Start();
        }

        private void KinectSensorChooserKinectChanged(object sender, KinectChangedEventArgs e)
        {
           // throw new NotImplementedException();
            //sensor = e.NewSensor;
            
        }

        private void Expander_MouseEnter(object sender, MouseEventArgs e)
        {
            if (!expander.IsExpanded) expander.IsExpanded = true;
            
        }

        private void Expander_MouseLeave(object sender, MouseEventArgs e)
        {

        }
    }
}
