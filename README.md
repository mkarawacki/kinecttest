Application for remotely controlling Lego Mindstorms robots - both NXT and EV3. Uses Bluetooth for RC functionality (32feet.NET & Monobrick libraries)

Prerequisites:

1. *MonoBrick Library* from [monobrick.dk](http://monobrick.dk)
2. *Kinect SDK version 1.8*
3. *Visual Studio 2013*
4. Either Windows 7 or 8/8.1

Tested on:

* Windows 8.1 x64
* Visual Studio 2013 Professional
* Hardware specs: AMD FX 8320, 16GB RAM, GeForce GTS 450 

Admin:

mapkarawacki[at]gmail.com

A project of [Student Society of Computer Science Enthusiasts](https://facebook.com/knpi.us) at the University of Silesia